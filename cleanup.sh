#!/usr/bin/env bash

directories=(
  "Cycles"
  "4K-Wallpapers"
)

for i in "${directories[@]}"; do
  cd projects
  cd "$i"
  ls | grep .blend1 | xargs -I {} rm {}
  cd ../..
done
